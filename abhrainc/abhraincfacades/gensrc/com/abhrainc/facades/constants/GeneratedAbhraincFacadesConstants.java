/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Mar, 2017 11:15:47 PM                     ---
 * ----------------------------------------------------------------
 */
package com.abhrainc.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAbhraincFacadesConstants
{
	public static final String EXTENSIONNAME = "abhraincfacades";
	
	protected GeneratedAbhraincFacadesConstants()
	{
		// private constructor
	}
	
	
}
