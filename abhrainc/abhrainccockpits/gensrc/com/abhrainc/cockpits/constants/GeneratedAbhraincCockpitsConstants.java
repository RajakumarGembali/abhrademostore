/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Mar, 2017 11:15:47 PM                     ---
 * ----------------------------------------------------------------
 */
package com.abhrainc.cockpits.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAbhraincCockpitsConstants
{
	public static final String EXTENSIONNAME = "abhrainccockpits";
	
	protected GeneratedAbhraincCockpitsConstants()
	{
		// private constructor
	}
	
	
}
