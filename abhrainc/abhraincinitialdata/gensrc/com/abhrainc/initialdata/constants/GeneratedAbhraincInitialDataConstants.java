/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Mar, 2017 11:15:47 PM                     ---
 * ----------------------------------------------------------------
 */
package com.abhrainc.initialdata.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAbhraincInitialDataConstants
{
	public static final String EXTENSIONNAME = "abhraincinitialdata";
	
	protected GeneratedAbhraincInitialDataConstants()
	{
		// private constructor
	}
	
	
}
