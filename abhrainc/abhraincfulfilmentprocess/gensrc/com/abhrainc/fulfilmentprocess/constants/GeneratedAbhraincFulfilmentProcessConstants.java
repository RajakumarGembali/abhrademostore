/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Mar, 2017 11:15:47 PM                     ---
 * ----------------------------------------------------------------
 */
package com.abhrainc.fulfilmentprocess.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAbhraincFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "abhraincfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedAbhraincFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
